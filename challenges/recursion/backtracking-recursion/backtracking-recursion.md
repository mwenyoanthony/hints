# Recursion: Backtracking Recursion

<!-- Hint 1-->
<details>
  <summary><b>Hint 1:</b>&nbsp;Basis of the problem</summary><br>

To solve this problem without any code, let's analyze the given inputs and the expected output.

```ruby
def exact_sum?(k, coins) end;
```

Inputs:<br>
&emsp;\- **k**, is an integer value which is the desired sum.<br>
&emsp;\- **coins**, is an array of decimals which we'll use its elements to meet **k** by addition.

Output:<br>
&emsp;\- If sum of any **sub-array** from coins array meet with "k", then return true. Otherwise, return false.

As the first inference, we'll need to find all the possible sub-arrays of coins array. Then, we can check if any sum of sub-array meets with the desired value. 

There is a subset of X that includes x and whose sum is T.
There is a subset of X that excludes x and whose sum is T.

For finding all the possible sub-arrays recursively we can divide each problem into two sub-problems:<br>
&emsp;- A sub-problem includes the first element of the array, recur for `array = array[1, array.length]`<br>
&emsp;- A sub-problem excludes the first element of the array, recur for `array = array[1, array.length]`<br>

<b>`Hint:`&nbsp;Implement the recursive function to find all the possible sub-arrays recursively.</b><br>
<b>`Hint:`&nbsp;Follow backtracking strategy and implement required base-cases to process each recursive level.<b/><br>
<b>`Hint:`&nbsp;No need to use another parameter to keep track each sub-array. You can substract first element from "k". Then when k equals zero, it means you've found the exact sum.</b>
  
<b>`Note:`</b>&nbsp;**[1, array.length]** or **array[1..-1]** can be used to access a slice of elements. For more information, check this [link](https://ruby-doc.org/core-2.6.1/Array.html#class-Array-label-Accessing+Elements)

</details>
<!-- Hint 1 -->

---

<!-- Hint 2 -->
<details>
  <summary><b>Hint 2:</b>&nbsp;Backtracking strategy</summary><br>

The recursive flow of finding sub-arrays as we discussed previously:<br>
<img src="images/recursion/backtracking-recursion/backtracking-recursion-1.png"  width="800"><br>

Backtracking is an important recursive strategy. It's faster than its brute-force equivalent. However, mostly backtracking solutions can be solved by dynamic programming efficiently.

According to definition in [Technopedia](https://www.techopedia.com/definition/17837/backtracking):

> Backtracking helps in solving an overall issue by finding a solution to the first sub-problem and then recursively attempting to resolve other sub-problems based on the solution of the first issue. If the current issue cannot be resolved, the step is backtracked and the next possible solution is applied to previous steps, and then proceeds further. In fact, one of the key things in  backtracking is recursion. It is also considered as a method of exhaustive search using divide and conquer. A backtracking algorithm ends when there are no more solutions to the first sub-problem.

For providing this computing logic, we need to keep track of current sub-array at the each recursive step. The recommended way to handle this logic is using "k" value as a mediator.

<b>`Hint:`&nbsp;When you include the first element, substract it from "k".</b><br>
<b>`Hint:`&nbsp;When you exclude the first element, no need to manipulate "k".</b><br>
<b>`Hint:`&nbsp;When you get k = 0 at a recursive step you've found the desired sum. Return true and stop the recursion. Otherwise, continue.</b>
  
<b>`Note:` If you don't feel comfortable with using "k" as a mediator of addition process, you can use additional parameter i.e current_sum for keep track of sum.<br>

</details>
<!-- Hint 2-->

---

<!-- Hint 3 -->
<details>
  <summary><b>Hint 3:</b>&nbsp;Structure of the recursive function</summary><br>

The recursive flow of the whole solution with **using "k" as a mediator**:<br>
<img src="images/recursion/backtracking-recursion/backtracking-recursion-2.png"  width="800"><br>

The recursive flow of the whole solution with using **additional sum paramater**:<br>
<img src="images/recursion/backtracking-recursion/backtracking-recursion-3.png"  width="800"><br>


There are two base cases:<br>
<b>`Hint:`&nbsp;If the target value "k" is zero, then we can immediately return true.<br>
<b>`Hint:`&nbsp;On the other hand, if current "k" < 0, or if the current sub-array is empty, then we can immediately return false<br>

There are two recursive calls:<br>
<b>`Hint:`&nbsp;First one will recur through the sub-array that includes first element of the current sub-array and whose sum is "k - coins[0]".<br>
<b>`Hint:`&nbsp;Second one will recur through the sub-array that excludes first element of the current sub-array and whose sum is will be the same, "k".<br>
  
</details>
<!-- Hint 3 -->

---

<!-- Solution 1 -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Solution (Recursive)</summary><br>

```ruby
def exact_sum?(k, coins)
  # if current sum equals zero, the current sub-array includes the exact sum!
  return true if k == 0
  # if the current sub-array is empty, or if the current sum less than zero
  # this branch doesn't include the exact sum
  return false if coins.empty? || k < 0
  # first recursive call recurs through with including first element of current sub-array, substract coins[0] from "k"
  # second recursive call recurs through with excluding first element of current sub-array
  exact_sum?(k - coins[0], coins[1,coins.length]) || exact_sum?(k, coins[1, coins.length])
end
```

Alternative solution with using additional parameter for holding sum:

```ruby
def exact_sum?(k, coins, sum = 0)
  # if current sum equals k, that's the exact sum! 
  return true if k == sum
  # same as described on above solution
  return false if coins.length == 0 || sum < 0
  # same as described on above solution except, we're incrementing sum at the each recursive step instead of substracting from "k"
  exact_sum?(k, coins[1,coins.length], sum + coins[0]) || exact_sum?(k, coins[1, coins.length], sum)
end
```

</details>
<!-- Solution 1 -->

---

<!-- Solution 2 -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Alternative solution (Recursive) - better space complexity</summary><br>

This solution uses index tracking instead of slicing array at the each recursive call. 

```ruby
def exact_sum?(k, coins, i = 0)
  # if current sum equals zero, the current sub-array includes the exact sum!
  return true if k == 0
  # if the current sub-array is empty, or if the current sum less than zero
  # this branch doesn't include the exact sum
  return false if i >= coins.length || k < 0

  exact_sum?(k, coins, i + 1) || exact_sum?(k - coins[i], coins, i + 1)
end
```
</details>
<!-- Solution 2 -->

